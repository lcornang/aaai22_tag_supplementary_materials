"""
Scoring script for RTI+ automata
Author: 
"""

import sys, os
import re
import tempfile

sys.path.append(os.path.abspath(os.path.dirname(sys.argv[0]))+"/TAG/")

from TAG.Automaton import Automaton

def rtip_to_dot(file, dotfile, proba=True):
    pdrta = open(file, "r")
    lines = pdrta.readlines()
    dot = ''
    for line in lines:
        if re.match('SOLUTION:', line) is not None: continue
        if re.match('SCORE', line) is not None: break
        m = re.search('^[\d]+(?=\s*)', line)
        source = 'S' + str(m.group(0))
        m = re.search('[\w]+(?=\s\[)', line)
        symbol = str(m.group(0))
        m = re.search('\[[\d]+, [\d]+\]', line)
        guard = m.group(0)
        guard = re.sub('[\[\]]', '', guard)
        guard = re.split(', ', guard)
        ini = guard[0]
        fin = guard[1]
        m = re.search('(?<=->)[^\s]+', line)
        destination = 'S' + str(m.group(0))
        if destination == 'S-1':
            destination = source
        if proba:
            p = re.search('(?<=p=)[\d.\d]+', line).group()
            dot = dot + source + ' -> ' + destination + ' [label="' + symbol + ' [' + str(ini) + ', ' + str(fin) + '] \\np=' + p + '"];' + '\n'
        else:
            dot = dot + source + ' -> ' + destination + ' [label="' + symbol + ' [' + str(ini) + ', ' + str(fin) + ']"];' + '\n'
    pdrta.close()
    dotfile = open(dotfile, "w")
    dotfile.write(dot)
    dotfile.close()

fo = tempfile.NamedTemporaryFile()
rtip_to_dot(sys.argv[1], fo.name)
ta = Automaton(fo.name)

tss_file = open(sys.argv[2])
tss = tss_file.readlines()
tss_file.close()
neg = []
for ts in tss:
    ts = re.sub('\\n', '', ts)
    ts = ts.split(' ')
    neg.append(ts)

res = ta.inconsistency_nb(neg, True, False, False)
print(res, end="|")

tss_file = open(sys.argv[3])
tss = tss_file.readlines()
tss_file.close()
pos = []
for ts in tss:
    ts = re.sub('\\n', '', ts)
    ts = ts.split(' ')
    pos.append(ts)

res = ta.inconsistency_nb(pos, True, False, False)
print(res)

fo.close()
