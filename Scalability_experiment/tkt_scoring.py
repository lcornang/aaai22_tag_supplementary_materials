"""
Scoring script for Timed k-Tail automata
Author: 
"""

import sys, os
import re

sys.path.append(os.path.abspath(os.path.dirname(sys.argv[0]))+"/TAG/")

from TAG.Automaton import Automaton

def next_edge(ta, last: str, symbol: str, time_value: int = None):
    source = ta.search_state(last)
    edges = list()
    for e in source.edges_out:
        if e.symbol == symbol:
            if min(e.guard) <= time_value <= max(e.guard): edges.append(e)
    return edges

def exist_path(ta,  ts: list, timed: bool, initial: str = 'S0') -> bool:
    seq_edges = []
    last = ta.search_state(initial)
    seq_states = [last]
    for i in range(0, len(ts[:-1])):
        pair = ts[i].split(':')
        edges = next_edge(ta, last.name, pair[0], eval(pair[1]))
        if len(edges) == 0: return False
        ok = False
        pair2 = ts[i+1].split(':')
        for e in edges:
            edges2 = next_edge(ta, e.destination.name, pair2[0], eval(pair2[1]))
            if len(edges2) > 0:
                ok = True
                break
        if not ok: return False
        last = e.destination
        seq_edges.append(e)
        seq_states.append(last)
    pair = ts[-1].split(':')
    edge = ta.next_edge(last.name, pair[0], eval(pair[1]))
    if edge is None: return False
    last = edge.destination
    seq_edges.append(edge)
    seq_states.append(last)
    return True

def is_consistent(ta, tss: list, timed: bool) -> int:
    consistent = True
    l = list()
    for ts in tss:
        exist = exist_path(ta, ts, timed)
        if not exist:
            l.append(tss.index(ts))
            consistent = False
    return len(l)

ta = Automaton(sys.argv[1])

tss_file = open(sys.argv[2])
tss = tss_file.readlines()
tss_file.close()
neg = []
for ts in tss:
    ts = re.sub('\\n', '', ts)
    ts = ts.split(' ')
    neg.append(ts)

res = is_consistent(ta, neg, True)
print(res, end="|")

tss_file = open(sys.argv[3])
tss = tss_file.readlines()
tss_file.close()
pos = []
for ts in tss:
    ts = re.sub('\\n', '', ts)
    ts = ts.split(' ')
    pos.append(ts)

res = is_consistent(ta, pos, True)
print(res)
