import sys, os

sys.path.append(os.path.abspath(os.path.dirname(sys.argv[0]))+"/TAG/")
sys.path.append(os.path.abspath(os.path.dirname(sys.argv[0]))+"/Automaton_generator/")

from TAG.Automaton import Automaton
from Automaton_generator.Generator import Generator
from Automaton_generator.TSSGenerator import TSSGenerator

state_nb = 10
split_ratio = 0.25
min_guard, max_guard = 0, 20
tss_nb = 500
alpha_size = 5

folder_path = os.path.abspath(os.path.dirname(sys.argv[0]))+'/2-outdegree/'
for outdegree in [1, 1.25, 1.5, 1.75, 2, 2.25, 2.5]:
    edge_nb = round(outdegree * state_nb)
    try:
        g = Generator(state_nb, edge_nb, alpha_size, split_ratio, min_guard, max_guard)
        g.generate(200)
        id = 0
        for ta in g.tas:
            path = folder_path + 'model_tas/' + 'ta-'+str(id)+'-'+str(alpha_size)+'-'+str(outdegree).replace('.', '_')+'-'+str(state_nb)+'-'+str(split_ratio).replace('.', '_')
            ta.export_ta(path)
            id += 1
        print("outdegree " + str(outdegree) + ", " + str(len(g.tas)) + " TA generated.")
    except:
        print("No TA generated for outdegree " + str(outdegree))

for outdegree in [1, 1.25, 1.5, 1.75, 2, 2.25, 2.5]:
    for id in range(0, 200):
        caracteristics = str(id)+'-'+str(alpha_size)+'-'+str(outdegree).replace('.', '_')+'-'+str(state_nb)+'-'+str(split_ratio).replace('.', '_')
        g = TSSGenerator(folder_path+'model_tas/'+'ta-'+caracteristics, tss_nb, p_fin=0.15)
        g.generate()
        g.tag_file_writer(folder_path+'traces-tag-'+caracteristics+'-'+str(tss_nb))
        g.tkt_file_writer(folder_path+'traces-tkt-'+caracteristics+'-'+str(tss_nb))
        g.rtip_file_writer(folder_path+'traces-rtip-'+caracteristics+'-'+str(tss_nb))
        g.n_tss = 100
        g.empty_tss()
        g.generate(False)
        g.tag_file_writer(folder_path+'traces-negatives-'+caracteristics)
        g.empty_tss()
        g.generate(True)
        g.tag_file_writer(folder_path+'traces-positives-'+caracteristics)
    print("Traces for outdegree "+str(outdegree)+" generated.")
