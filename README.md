# Supplementary Materials for "TAG: Learning Timed Automata from Logs" (AAAI-22).

Authors: Lénaïg Cornanguer, Christine Largouët, Laurence Rozé, Alexandre Termier.

<h2 id="contribution">Contribution</h2>
<h3 id="tag-source-code">TAG source code (<font color="#72A6AE">TAG</font> folder)</h3>
<p>TAG is implemented in Python (version: 3.8.3). It requires the following modules: graphviz (0.16), IPython (7.21.0). TAG's source code is available in the following repository: https://gitlab.inria.fr/lcornang/tag/. To run the experiment, TAG (commit 19a2c8bb) must be included in the experiment folders (copy Automaton.py, State.py, Edge.py, TALearner.py in the folder named "TAG" in each experiment folder). </p>

<h3 id="proofs-and-pseudo-code">Proofs and pseudo-code <font color="#72A6AE">(proofs_algos.pdf)</font></h3>
<p>Complete pseudo-code of TAG and its proofs are included in a PDF file. This appendix will be included in an online supplementary materials folder (with the experiment materials) dedicated to this paper, linked in the article.</p>

<h2 id="experiments">Experiments</h2>
<h3 id="experiment-on-synthetic-data">Experiment on synthetic data</h3>
<p>The resources for the experiment on synthetic data will be included in an online supplementary materials folder dedicated to this paper, linked in the article. Due to the number of model automata, traces, and resulting automata, the folder size was too important for the submission website. For this reason, we only supply one example of model automaton, traces, and result <font color="#72A6AE">(Scalability_experiment/sample_data)</font>. However, the files containing the results (scores, runtime...) were kept to allow their analysis, along with all the files used to generate the data.</p>
<h4 id="how-to-run">How to run?</h4>
<p>Factor by factor (e.g. alphabet size):<br>
<strong>Step 1-</strong> Generate the model automata and the traces with the script corresponding to the factor <font color="#72A6AE">(Scalability_experiment/<i>factor_name</i>_generation_at.py)</font>. Model automata will be named <font color="#72A6AE">ta-{id}-{alphabet size}-{outdegree}-{state number}-{twin-transition ratio}</font> and be in DOT format. Learning traces will be named <font color="#72A6AE">traces-{tag/rtip/tkt}-{id}-{alphabet size}-{outdegree}-{state number}-{twin-transition ratio}-{timed words number}</font>, and validation traces <font color="#72A6AE">traces-{positives/negatives}-{tag/rtip/tkt}-{id}-{alphabet size}-{outdegree}-{state number}-{twin-transition ratio}</font>.<br>
<strong>Step 2-</strong> Learn the automata by executing the shell script corresponding to the factor <font color="#72A6AE">(Scalability_experiment/<i>factor_name</i>.sh)</font>. The learned TA will be named <font color="#72A6AE">res-{tag/rtip/tkt}-{id}-{alphabet size}-{outdegree}-{state number}-{twin-transition ratio}-{timed words number}</font><br>
After every factor done:<br>
<strong>Step 3-</strong> Analyze the results with the Shiny App <font color="#72A6AE">(Scalability_experiment/app.R)</font>.<br>
</p>
<h4 id="ressources">Ressources description</h4>
<p><strong>Shell script per factor</strong> <font color="#72A6AE">Scalability_experiment/factor_name.sh</font><br>
Shell scripts launching the different algorithms for the learning process and the validation process with all the combinations of factors for a given factor. These shell scripts use "gdate", please replace by "date" when executed on Linux/Cygwin terminal. These shell scripts use "graphviz", please install graphviz to execute.  These shell scripts use "bc", please install "bc" if executed on Cygwin terminal. 

<p><strong>State-of-the-art TA learners</strong><br>
<em>Timed k-Tail</em> was modified in order to output its learned automaton in a specific format for the sake of the automatization of the experiment (addition of a function to export the learned TA formatted in a file). Source code (Scalability_experiment/tkt_master) and compiled version <font color="#72A6AE">(Scalability_experiment/tkt.jar)</font> are provided.<br>
<em>RTI+</em> was modified to stop its execution after the first TA output, as it can continue its search for hours <font color="#72A6AE">(Scalability_experiment/RTI)</font>. To compile, use "make debug".</p>

<p><strong>Model automata and traces generator</strong> <font color="#72A6AE">(Scalability_experiment/Automata_generator)</font><br>
Modules used by the <font color="#72A6AE">factor_name</i>_generation_at.py</font> script to generate the model automata, learning traces, and positive and negative traces for validation.</p>

<p><strong>Validation scripts</strong> <font color="#72A6AE">(Scalability_experiment/<em>{tkt/rtip/tag}</em>_scoring.py)</font><br>
Python script to check if the validation traces are accepted by the resulting automata (to further compute their scores).</p>

<p><strong>Results data</strong> <font color="#72A6AE">(Scalability_experiment/<i>factor_name</i>/results.csv)</font><br>
CSV file containing the scores and runtime for each set of traces and each learner. /!\ The data contained in these files are deleted at each execution of the corresponding learning shell script.

<h3 id="experiment-on-real-data">Experiment on real data</h3>
<p>The TV logs can be download from the website cited in the article. The logs used for the experiment are included in this supplementary materials folder (july: <font color="#72A6AE">TV_logs_experiement/2020-08\CBET_202008_140345885.log</font>, august: <font color="#72A6AE">TV_logs_experiement/2020-07\CBET_202007_125607393.log</font>). 
UPPAAL (version 64-4.1.24) can be downloaded on UPPAAL homepage.

<h4 id="friday-morning">Friday morning</h4>
<font color="#72A6AE">TV_logs_experiement/generation_tag_fri_am.py</font> contains the script used to generate the traces of the Friday mornings <font color="#72A6AE">(TV_logs_experiement/traces_tag_fri_am)</font>. It requires the following packages: numpy (1.20.1) and pandas (1.3.2).

<h4 id="weekdays">Weekdays</h4>
TV_logs_experiement/generation_summer.py contains the script used to generate the traces of the summer months <font color="#72A6AE">(TV_logs_experiement/traces_tag_summer)</font> and the xml file for UPPAAL <font color="#72A6AE">(traces_tag_summer.xml)</font>. It requires the following packages: numpy (1.20.1) and pandas (1.3.2).

<h2 id="requirements">Requirements (summary)</h2>
<h3 id="python">Python version + packages</h3>
<ul>
	<li>Python 3.8.3</li>
	<li>Numpy 1.20.1</li>
	<li>Pandas 1.3.2</li>
	<li>graphviz 0.16</li>
	<li>IPython 7.21.0</li>
</ul>

<h3 id="r">R version + packages</h3>
<ul>
	<li>R 4.0.3</li>
	<li>ggplot2 3.3.2</li>
	<li>tidyverse 1.3.0</li>
	<li>stringr 1.4.0</li>
	<li>ggpubr 0.4.0</li>
	<li>car 3.0-10</li>
	<li>rstatix 0.6.0</li>
	<li>patchwork 1.1.1</li>
</ul>

<h3 id="others">Softwares</h3>
<ul>
	<li>UPPAAL 64-4.1.24</li>
	<li>Graphviz 2.40.1-5</li>
</ul>

<h3 id="windows">For Windows users</h3>
<ul>
	<li>Cygwin 3.2.0</li>
	<li>bc 1.06.95-2</li>
</ul>

</div>
